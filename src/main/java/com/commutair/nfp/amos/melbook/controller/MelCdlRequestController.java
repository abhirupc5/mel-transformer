package com.commutair.nfp.amos.melbook.controller;

import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.CORRELATION_ID_CONSTANT;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.nfp.amos.melbook.domain.MelBookDomain;
import com.commutair.nfp.amos.melbook.domain.Response;
import com.commutair.nfp.amos.melbook.entity.MelCdlRequest;
import com.commutair.nfp.amos.melbook.transformer.MelCdlRequestPublisher;
import com.commutair.nfp.amos.melbook.transformer.MelCdlRequestTransformer;

@RestController
public class MelCdlRequestController {
	
	private Logger logger = LoggerFactory.getLogger(MelCdlRequestController.class);
	
	@Autowired
	private MelCdlRequestTransformer transformer;
	
	@Autowired
	private MelCdlRequestPublisher publisher;
	
	/**
	 * @param payload
	 * @return
	 */
	@RequestMapping(value = "/amos/transform", method = RequestMethod.POST)
	public ResponseEntity<Response> transform(@RequestHeader("correlationId") final String correlationId, final @RequestBody String payload) {
		MDC.put(CORRELATION_ID_CONSTANT, "[" + correlationId + "]");
		logger.info(String.format("Consumed message %s", payload));
		Response response = null;
		try {
			final MelBookDomain melBookDomain = transformer.deserialize(payload, correlationId);
			
			final List<String> validationMessages = transformer.isValid(melBookDomain);
			if(validationMessages.size() > 0) {
				publisher.publishException(validationMessages.get(0), correlationId);
				response = new Response(0, validationMessages.get(0), HttpStatus.BAD_REQUEST);
				response.setCorrelationId(correlationId);
				return ResponseEntity.badRequest().body(response);
			}
			
			final MelCdlRequest transformedEntity = transformer.transform(melBookDomain);
			
			final String melCdlRequestXml = transformer.serialize(transformedEntity);
			
			response = new Response(0, melCdlRequestXml, HttpStatus.ACCEPTED);
			response.setCorrelationId(correlationId);
		} catch (Exception e) {
			response = new Response(0, e.getMessage(), HttpStatus.BAD_REQUEST);
			response.setCorrelationId(correlationId);
		}
		return ResponseEntity.ok().body(response);
	}
}
