package com.commutair.nfp.amos.melbook.domain;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

public class Response {
	private String timestamp;
	private int status;
	private int code;
	private String message;
	private String correlationId;

	public Response(int code, String message, HttpStatus status) {
		this.timestamp = LocalDateTime.now().toString();
		this.code = code;
		this.message = message;
		this.status = status.value();
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	
}
