package com.commutair.nfp.amos.melbook.util;

import java.util.regex.Pattern;

public class MelCdlConstants {
	
	public static final String BUSINESS_KEY_SEPARATOR = "-";
	
	public static final String MEL_CONSTANT = "MEL";
	
	public static final String CDL_CONSTANT = "CDL";
	
	public static final String EMPTY_STRING = "";
	
	public static final String AIRCRAFT_ICAOCODE_CONSTANT = "E145";
	
	public static final String WHOALC_CONSTANT = "UCA";
	
	public static final String BUSINESSKEY_REGEX = "MelBook::([\\\\d-]+)";
	
	public static final Pattern BUSINESS_KEY_PATTERN = Pattern.compile("MelBook::([\\w-]+)");
	
	public static final String CORRELATION_ID_CONSTANT = "correlationId";
}
