package com.commutair.nfp.amos.melbook.transformer;

public interface MelCdlRequestPublisher {
	
	/**
	 * This method is responsible for publishing exception to queue
	 * 
	 * @param payload
	 */
	public void publishException(final String exception, final String correlationId);
}
