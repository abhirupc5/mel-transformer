package com.commutair.nfp.amos.melbook.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MelBookInputDomain {

	@JsonProperty("eventName")
	private String eventName;
	
	@JsonProperty("NewImage")
	private MelBookDomain newImage;
	
	@JsonProperty("OldImage")
	private MelBookDomain oldImage;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public MelBookDomain getNewImage() {
		return newImage;
	}

	public void setNewImage(MelBookDomain newImage) {
		this.newImage = newImage;
	}

	public MelBookDomain getOldImage() {
		return oldImage;
	}

	public void setOldImage(MelBookDomain oldImage) {
		this.oldImage = oldImage;
	}
}
