package com.commutair.nfp.amos.melbook.util;

import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.BUSINESS_KEY_SEPARATOR;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.CDL_CONSTANT;

import java.util.stream.Stream;

import com.commutair.nfp.amos.melbook.entity.MelCdlTypeEnum;

public class MelCdlUtils {

	/**
	 * @param val
	 * @return
	 */
	public static MelCdlTypeEnum getMelCdlTypeEnum(final String val) {
		return Stream.of(MelCdlTypeEnum.values()).filter(type -> type.name().equals(val)).findFirst().get();
	}
	
	/**
	 * This method is responsible for checking if the string is numeric
	 * 
	 * @param val
	 * @return
	 */
	public static boolean isNumeric(final String val) {
		boolean isValid = true;
		if(val == null) {
			isValid = false;
		} else {
			try {
				Integer.parseInt(val);
			} catch (Exception e) {
				isValid = false;
			}
		}
		return isValid;
	}
	
	/**
	 * @param businessKey
	 * @return
	 */
	public static String getMelCdlCode(final String businessKey) {
		if(businessKey.endsWith(CDL_CONSTANT)) {
			return businessKey.substring(0, businessKey.lastIndexOf(BUSINESS_KEY_SEPARATOR));
		}
		return businessKey;
	}
}
