package com.commutair.nfp.amos.melbook.transformer.impl;

import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.AIRCRAFT_ICAOCODE_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.BUSINESS_KEY_PATTERN;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.CDL_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.MEL_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.WHOALC_CONSTANT;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Service;

import com.commutair.nfp.amos.melbook.domain.MelBookDomain;
import com.commutair.nfp.amos.melbook.domain.MelBookInputDomain;
import com.commutair.nfp.amos.melbook.entity.AddMelCdlInput;
import com.commutair.nfp.amos.melbook.entity.AircraftType;
import com.commutair.nfp.amos.melbook.entity.MelCdl;
import com.commutair.nfp.amos.melbook.entity.MelCdlId;
import com.commutair.nfp.amos.melbook.entity.MelCdlList;
import com.commutair.nfp.amos.melbook.entity.MelCdlRequest;
import com.commutair.nfp.amos.melbook.transformer.MelCdlRequestTransformer;
import com.commutair.nfp.amos.melbook.util.MelCdlConstants;
import com.commutair.nfp.amos.melbook.util.MelCdlUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MelCdlRequestTransformerImpl implements MelCdlRequestTransformer {

	@Override
	public MelCdlRequest transform(final MelBookDomain melBookDomain) throws Exception {
		MelCdlRequest melCdlRequest = new MelCdlRequest();
		final AddMelCdlInput input = new AddMelCdlInput();
		final MelCdlList melCdlList = new MelCdlList();
		final MelCdl melCdl = new MelCdl();
		final MelCdlId melCdlId = new MelCdlId();
		final Matcher matcher = BUSINESS_KEY_PATTERN.matcher(melBookDomain.getBusinessKey());
		final String businessKey = matcher.find() ? matcher.group(1) : MelCdlConstants.EMPTY_STRING;
		melCdlId.setType(businessKey.endsWith(CDL_CONSTANT) ? MelCdlUtils.getMelCdlTypeEnum(MEL_CONSTANT) : MelCdlUtils.getMelCdlTypeEnum(CDL_CONSTANT));
		melCdlId.setValue(MelCdlUtils.getMelCdlCode(businessKey));
		melCdl.setMelCdlId(melCdlId);
		AircraftType aircraftType = new AircraftType();
		aircraftType.setIcaoCode(AIRCRAFT_ICAOCODE_CONSTANT);
		melCdl.setAircraftType(aircraftType);
		melCdl.setWhoAlc(WHOALC_CONSTANT);
		melCdlList.getMelCdl().add(melCdl);
		input.setMelCdlList(melCdlList);
		melCdlRequest.setAddMelCdl(input);
		return melCdlRequest;
	}

	@Override
	public MelBookDomain deserialize(final String payload, final String correlationId) throws Exception {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		final MelBookDomain melBookDomain = mapper.readValue(payload, MelBookInputDomain.class).getNewImage();
		melBookDomain.setCorrelationId(correlationId);
		return melBookDomain;
	}

	@Override
	public String serialize(final MelCdlRequest melCdlRequest) throws Exception {
		JAXBContext jc = JAXBContext.newInstance(MelCdlRequest.class);
		Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter sw = new StringWriter();
        marshaller.marshal(melCdlRequest, sw);
		return sw.toString();
	}

	@Override
	public List<String> isValid(MelBookDomain domain) throws Exception {
		List<String> invalidMessage = new ArrayList<>();
		final Matcher matcher = BUSINESS_KEY_PATTERN.matcher(domain.getBusinessKey());
		if(!matcher.find()) {
			invalidMessage.add(String.format("Invalid format for businessKey (%s)", domain.getBusinessKey()));
		}
		return invalidMessage;
	}
}


