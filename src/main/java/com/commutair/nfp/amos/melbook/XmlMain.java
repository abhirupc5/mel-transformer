package com.commutair.nfp.amos.melbook;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.commutair.nfp.amos.melbook.domain.QueuePayload;
import com.fasterxml.jackson.core.JsonProcessingException;

public class XmlMain {

	public static void main(String[] args) throws JsonProcessingException, JAXBException {
		QueuePayload payload = new QueuePayload();
		payload.setCorrelationId("abc");
		payload.setError("xyz");
		payload.setPayload("aaaaa");
		
		JAXBContext jc = JAXBContext.newInstance(QueuePayload.class);
		Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter sw = new StringWriter();

        marshaller.marshal(payload, sw);
        System.out.println(sw.toString());
	}

}
