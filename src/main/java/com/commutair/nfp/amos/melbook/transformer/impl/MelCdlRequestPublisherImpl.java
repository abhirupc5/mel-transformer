package com.commutair.nfp.amos.melbook.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

import com.commutair.nfp.amos.melbook.domain.QueuePayload;
import com.commutair.nfp.amos.melbook.transformer.MelCdlRequestPublisher;

@Service
public class MelCdlRequestPublisherImpl implements MelCdlRequestPublisher {

	@Autowired
	private QueueMessagingTemplate messagingTemplate;
	
	@Value("${melcdlexception-queue-url}")
	private String exceptionQueue;
	
	@Override
	public void publishException(final String exception, final String correlationId) {
		QueuePayload queuePayload = new QueuePayload();
		queuePayload.setError(exception);
		queuePayload.setCorrelationId(correlationId);
		messagingTemplate.convertAndSend(exceptionQueue, queuePayload);
	}
}
