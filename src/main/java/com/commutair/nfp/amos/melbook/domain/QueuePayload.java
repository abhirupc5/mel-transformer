package com.commutair.nfp.amos.melbook.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueuePayload {
	@JsonProperty("correlationId")
	private String correlationId;
	
	@JsonProperty("payload")
	private String payload;
	
	@JsonProperty("error")
	private String error;

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
