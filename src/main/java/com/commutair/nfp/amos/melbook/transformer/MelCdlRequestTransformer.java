package com.commutair.nfp.amos.melbook.transformer;

import java.util.List;

import com.commutair.nfp.amos.melbook.domain.MelBookDomain;
import com.commutair.nfp.amos.melbook.entity.MelCdlRequest;

public interface MelCdlRequestTransformer {

	/**
	 * This method is responsible for transforming MelBookDomain to MelCdlRequest entity class.
	 * 
	 * @param melBookEntity
	 * @return
	 */
	public MelCdlRequest transform(final MelBookDomain melBookEntity) throws Exception;
	
	/**
	 * This method is responsible for deserializing payload to MelBookDomain class 
	 * 
	 * @param payload
	 * @param correlationId 
	 * @return
	 */
	public MelBookDomain deserialize(final String payload, final String correlationId) throws Exception;
	
	/**
	 * This method is responsible for validating domain 
	 * 
	 * @param payload
	 * @return
	 */
	public List<String> isValid(final MelBookDomain domain) throws Exception;
	
	/**
	 * This method is responsible for serializing MelCdlRequest entity to xml 
	 * 
	 * @param payload
	 * @return
	 */
	public String serialize(final MelCdlRequest melCdlRequest) throws Exception;
}
