package com.commutair.nfp.amos.melbook.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MelBookDomain {
	
	@JsonIgnore
	private String correlationId;

	@JsonProperty("ataChapter")
	private String ataChapter;
	
	@JsonProperty("businessKey")
	private String businessKey;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("revision")
	private String revision;
	
	@JsonProperty("sequenceNo")
	private String sequenceNo;
	
	@JsonProperty("transactionDateTime")
	private String transactionDateTime;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("itemNo")
	private String itemNo;
	
	@JsonProperty("qtyRequired")
	private String qtyRequired;

	public String getAtaChapter() {
		return ataChapter;
	}

	public void setAtaChapter(String ataChapter) {
		this.ataChapter = ataChapter;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getQtyRequired() {
		return qtyRequired;
	}

	public void setQtyRequired(String qtyRequired) {
		this.qtyRequired = qtyRequired;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
}
